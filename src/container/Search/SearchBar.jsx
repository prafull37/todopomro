import React,{Component} from 'react';
import SearchSvg from '../../../public/assets/Search/search-svg.svg';
import '../../../css/container/Search/searchbar.css';

class SearchBar extends Component{
    render(){
        return(
            <div>
                <div className="search-container" 
                // contentEditable={true}
                >
                    <div id="searchicon"></div>
                    </div>
                    <input type="text" placeholder="Search Your Task..." id="search"/>
                    {/* <span>
                        <img src={SearchSvg} alt="search-button" />
                    </span> */}
                {/* </div> */}
            </div>
        );
    }
        
}

export default SearchBar;