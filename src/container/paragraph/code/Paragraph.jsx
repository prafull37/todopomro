import React from 'react';
import PropTypes from 'prop-types';
import '../../../../css/container/paragraph/paragraph.css';

function Paragraph(props){
    return(
        <div className={`paragraph   ${props.className}`}>
            <p className={`margin-0  ${props.fontName} fontSize-${props.fontSize}`}>{props.para}<br/>
            {props.move&&<span className="right">{"- Prafull K. Singh"}</span>}
            </p>
                
        </div>
    );
}

Paragraph.propTypes={
    author:PropTypes.string,
    className:PropTypes.string.isRequired,
    fontName:PropTypes.oneOf(["Bree","PlayFair-Display"]),
    fontSize:PropTypes.number,
    move:PropTypes.bool.isRequired,
    para:PropTypes.string.isRequired,
}

export default Paragraph;