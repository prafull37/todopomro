import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Paragraph from './Paragraph.jsx'




class ParaContainer extends Component{
    constructor(props){
        super(props);

        this.state={
            // move:true,
        }
    }


    render(){
        const {move}= this.props;
        const para= this.props.para? this.props.para:'';

        return(
            <div className="para-container">
                {/* {console.log(!move)} */}
                {/* {!move ?  */}
                {/* ( */}
                    <Paragraph 
                            move={false}
                            fontSize={15}
                            fontName='PlayFair-Display'
                            para={para}
                            className="non-movable-paragraph"
                            />
                            {/* ) */}
                            {/* : */}
                            {/* ( */}
                                <Paragraph 
                                move={true}
                                fontSize={14}
                                fontName='Bree'
                                para={para}
                                className="movable-paragraph"
                                />
                                {/* ) */}
                                {/* } */}

                
            </div>
        );
    }
}

ParaContainer.propTypes={
    move:PropTypes.bool.isRequired,
    para:PropTypes.string,
}

export default ParaContainer;