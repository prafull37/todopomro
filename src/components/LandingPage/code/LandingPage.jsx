import React,{Component} from 'react';
import PageTemplate from '../../common/Template/code/PageTemplate.jsx';
import '../../../../css/component/LandingPage/landingpage.css';
import HeaderTitle from '../../common/Header/code/HeaderTittle.jsx';
import ParaContainer from '../../../container/paragraph/code/ParaContainer.jsx';
import {SecondParagraph} from '../../../constants/AppConstants.js'

import Button from '../../common/Button/code/Button.jsx'

class LandingPage extends Component{

   
    render(){
        return(
          <PageTemplate>
              <div className="lg-background">
                 
               <HeaderTitle 
                title="Romantic Title"
                className="title-in-page"
                />

                <ParaContainer 
                move={true}
                para={SecondParagraph} />
              </div>

              <Button/>

          </PageTemplate>
        );
    }
}

export default LandingPage;