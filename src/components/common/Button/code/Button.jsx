import React from 'react';
import '../../../../../css/common/Button/button.css'



const Button = ()=>{
    return (
        <div className="button-containers">
            <div className="button-after"/>
            <button className="button">
               <p id="button-text">Create </p> 
            </button>
        </div>
    );
}

export default Button;