import React,{Component} from 'react';
import PropTypes from 'prop-types';
import {getLogo} from '../../../../utils/getLogo.js';
import '../../../../../css/common/Header/headerlink.css';

class HeaderLink extends Component{

    constructor(props){
        super(props);
        this.state={
            isActive:false
        }
    }

    handle=()=>{
        
        this.setState({
            isActive:!this.state.isActive
        })
    }

render(){
    const {isLogo,logo,text}=this.props;
    const {isActive}=this.state;
    const LogoName=getLogo((isLogo?logo:null));

    return(
        <div onClick={this.handle} className="links-style">
            <button className="link-button" > 
                <div>
                {isLogo &&
                    <img src= {isActive?LogoName.active:LogoName.unactive} alt={isActive?`${logo}active`:`${logo}unactive`} className="headerImage"/>
                }
               <span className="link-text Montserrat">
                   {text}
               </span>
               </div>
            </button>
        </div>
    );
}
}

HeaderLink.propTypes={
    isLogo:PropTypes.bool.isRequired,
    logo:PropTypes.oneOf(['calendar','heart','login']),
    text:PropTypes.string.isRequired,
    
}

export default HeaderLink;