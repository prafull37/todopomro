import React,{Component} from 'react';
import PropTypes from 'prop-types';
import '../../../../../css/common/Header/headertitle.css'

function HeaderTitle(props) {
        
        const {title}=props;
        const hasClass=props.className?props.className:"";
        return(
        <h2 className={hasClass}>{title}</h2>
        );
    
}

HeaderTitle.propTypes={
    className:PropTypes.string,
    title:PropTypes.string.isRequired,
}

export default HeaderTitle;