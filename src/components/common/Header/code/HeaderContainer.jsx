import React,{Component,Fragment} from 'react';
import HeaderTitle from  './HeaderTittle.jsx';
import HeaderLink from  './HeaderLink.jsx'
import SearchBar from '../../../../container/Search/SearchBar.jsx'
import '../../../../../css/common/Header/headercontainer.css'

class HeaderContainer extends Component{

    render(){
        return(
            <div className="main-header-book">
            <header className="header-class">
                <Fragment>
                    <div className="header-title">
                        <HeaderTitle
                            title="Romantic Title"
                        />
                    </div>

                    <div className="header-search">
                     <SearchBar/>
                    </div>

                    <div className="header-links">
                    <HeaderLink 
                        isLogo={true}
                        logo="heart"
                        text="Calendar"
                    />
                    <HeaderLink 
                        isLogo={true}
                        logo="heart"
                        text="Archives"
                    />
                     <HeaderLink 
                        isLogo={true}
                        logo="heart"
                        text="Login"
                    />
                    </div>
                </Fragment>
               
            </header>
            <div id="headerline"/>
            <div id="brownline"/>
            </div>
           
        );
    }
}

export default HeaderContainer;