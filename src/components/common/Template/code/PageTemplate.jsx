import React, { Fragment } from 'react';
import HeaderContainer from '../../Header/code/HeaderContainer.jsx';

function PageTemplate(props){
    return(
        <Fragment key="page-template">
            <HeaderContainer/>
            {props.children}
        </Fragment>
    );
}

export default PageTemplate;