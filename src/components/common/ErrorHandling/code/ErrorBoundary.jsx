import React,{Component} from 'react';
import ErrorTemplate from './ErrorTemplate.jsx';
import PageTemplate from '../../Template/code/PageTemplate.jsx';


class ErrorBoundary extends Component{
    constructor(props){
        super(props);
        this.state={
            hasError:false
        }
    }

    componentDidCatch(error,errorInfo){
        this.setState({
            hasError:true
        })
        this.logger(error);
        this.logger(errorInfo);
    }

    logger=(isError)=>{
        console.log(isError)
    }

    render(){
        const {hasError}=this.state;
        if(hasError){
            return(
                <PageTemplate>
                    <ErrorTemplate/>
                </PageTemplate>
            );
        }

        return this.props.children;
    };
}


export default ErrorBoundary;