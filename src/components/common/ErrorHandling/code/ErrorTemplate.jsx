import React from 'react';
import '../../../../../css/common/ErrorHandling/errortemplate.css';

function ErrorTemplate(props){
    return(
        <div>
            <div id="error-message">
                <div id="message-div">
                <h3 id="error-header">We are facing some technical difficulties</h3>
                <p id="error-text"> Oops! Keep Calm our Warriors are Fighting to solve the issue </p>
                </div>
            </div>
        </div>
    );
    
}

export default ErrorTemplate;