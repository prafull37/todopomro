import React from 'react';
import LandingPage from './components/LandingPage/code/LandingPage.jsx';
import '../css/app.css'
import ErrorBoundary from './components/common/ErrorHandling/code/ErrorBoundary.jsx';

function App() {
  return (
    <ErrorBoundary>
    <LandingPage/>
    </ErrorBoundary>
  );
}

export default App;
